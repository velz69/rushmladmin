import { AfterViewInit, Directive, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from "@angular/router";
import { HttpService, RequestMethod } from "../services/http.service";
import { IGetCardResponse, IListResponse } from "../services/auth.service";
import { MatDialogConfig } from "@angular/material/dialog/dialog-config";
import { MatDialog } from "@angular/material/dialog";
import { MatDialogRef } from "@angular/material/dialog/dialog-ref";
import { MarketConfigCardComponent } from "../app/ml/market-config/market-config-card/market-config-card.component";
import { NotificationService } from "../services/notification.service";


@Directive()
export class BaseGridComponent implements AfterViewInit {
  public displayedColumns: string[];
  public dataSource!: MatTableDataSource<any>;
  private _selectedRowId: number | null = null;

  constructor(
    public dialog: MatDialog,
    public activatedRoute: ActivatedRoute,
    public notification: NotificationService,
    protected httpService: HttpService,
    protected _router: Router,
  ) {
    this.displayedColumns = this.prepareColumns();
    this.initToolbar();
  }

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  ngAfterViewInit(): void {
    this.initDataSource();
    this.customizeComponent();
  }

  public customizeComponent(): void {

  }

  public prepareColumns(): string[] {
    return []
  }

  public getEntityName(): string {
    return ""
  }

  public initToolbar(): void {}


  public initDataSource(): void {
    let response = this.httpService.requestML(this.getEntityName() + "/list", RequestMethod.GET);

    response.subscribe((data: IListResponse) => {
      if (data.error) {
        this.notification.warning(data.error);
      }
      if (data.success) {
        this.dataSource = new MatTableDataSource<any>(data.result)
        this.dataSource.paginator = this.paginator;
      }
    });
  }

  public getSelectedRowId(): number | null {
    return this._selectedRowId;
  }

  public onSelected(row: any): void {
    this._selectedRowId = row.id;
  }

  public triggerSelectedClass(row: any): boolean {
    return row.id == this._selectedRowId;
  }

  public checkBoxWidget(value: boolean): string {
    if (value) {
      return "<img src='/assets/icons/icon-checked.png' alt='image'>";
    }
    return "<img src='/assets/icons/icon-unchecked.png' alt='image'>";
  }

  public viewRedirect(row: any): void {
    this._router.navigate(["/api/" + this.getEntityName() + "/" + row.id]).then();
  }
}


@Directive()
export class BaseCrudGridComponent extends BaseGridComponent {
  protected isCardOpened: boolean = false;
  public card!: MatDialogRef<MarketConfigCardComponent>;

  public getCardComponent(): any {
    return
  }

  openCard(options?: MatDialogConfig): void {
    this.isCardOpened = true;

    this.card = this.dialog.open(this.getCardComponent(), options);

    this.card.afterOpened().subscribe(result => {
      this.afterCardOpened(result);
    })

    this.card.afterClosed().subscribe(result => {
      this.afterCardClosed(result);
    });
  }

  public afterCardOpened(result: any): void {
  }

  public afterCardClosed(result: any): void {
    this.isCardOpened = false;

    if (result) {
      this.initDataSource();
    }
  }

  public create(): void {
    if (!this.isCardOpened) {
      this.openCard();
    } else {
      this.notification.warning("Another card was already opened");
    }
  }

  public update(): void {
    if (this.isCardOpened) {
      this.notification.warning("Another card was already opened");
      return
    }

    let response = this.httpService.requestML(
      this.getEntityName() + "/update",
      RequestMethod.GET, {}, {"id": this.getSelectedRowId()}
    );

    response.subscribe((response: IGetCardResponse) => {
      if (response.error) {
        this.notification.warning(response.error);
      } else {
        this.openCard({data: response.data});
      }
    });
  }

  public delete(): void {
    if (this.isCardOpened) {
      this.notification.warning("Another card was already opened");
      return
    }

    let response = this.httpService.requestML(
      this.getEntityName() + "/delete",
      RequestMethod.POST, {"id": this.getSelectedRowId()}
    );

    response.subscribe((data: IListResponse) => {
      if (data.error) {
        this.notification.warning(data.error);
      }
      if (data.success) {
        this.initDataSource();
      }
    });
  }
}
