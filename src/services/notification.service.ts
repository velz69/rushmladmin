import { Injectable } from '@angular/core';
import { MatSnackBar } from "@angular/material/snack-bar";

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  durationInSeconds = 2;

  constructor(
    private _snackBar: MatSnackBar
  ) { }

  public notificate(message: string): void {
    this._snackBar.open(message, "X",{
      duration: this.durationInSeconds * 1000,
      horizontalPosition: "center",
      verticalPosition: "top",
      panelClass: ["notificate"]
    });
  }

  public warning(message?: string): void {
    this._snackBar.open(message ? message : "Unknown error", 'X',{
      horizontalPosition: "center",
      verticalPosition: "top",
      panelClass: "warning"
    });
  }
}
