import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { catchError, map, Observable } from "rxjs";
import { environment } from "../environments/environment";


export const RequestMethod = {
  GET: "GET",
  POST: "POST"
}

export interface IHeader {
  name: string;
  value: string;
}

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private _adminService: string;
  private _mlService: string;
  private _headers: HttpHeaders;
  private _baseURL: string = "/api/dashboard";

  constructor(
    private _http: HttpClient
  ) {
    this._adminService = environment.adminService;
    this._mlService = environment.mlService;
    this._headers = new HttpHeaders();
  }

  public errorMsg(text: string): void {

  }

  public setHeader(name: string, value: any): void {
    this._headers.set(name, value);
  }

  public requestML(
    path: string, method: string = RequestMethod.GET, post_data = {}, get_params = {}, custom_headers?: IHeader[]
  ): Observable<any> {
    return this._sendRequest(this._mlService, path, method, post_data, get_params, custom_headers);
  };

  public requestAdmin(
    path: string, method: string = RequestMethod.GET, post_data = {}, get_params = {}, custom_headers?: IHeader[]
  ): Observable<any> {
    return this._sendRequest(this._adminService, path, method, post_data, get_params, custom_headers);
  }

  public prepareURL(domen: string, path: string): string {
    let result = domen + this._baseURL;

    if (path[0] != "/") {
      result += "/";
    }

    result += path;

    return result
  }

  private _sendRequest(
    domen: string, path: string, method: string, post_data = {}, get_params = {}, custom_headers?: IHeader[]
  ): Observable<any> {
    let request;

    if (method == RequestMethod.GET) {
      request = this._get(domen, path, get_params, custom_headers);
    } else {
      request = this._post(domen, path, post_data, get_params, custom_headers);
    }

    return this._executeRequest(request);
  }

  private _executeRequest(request: Observable<Object>): Observable<string> {
    return request.pipe(map((data: any)=> {
      return this._prepareResponseData(data);
    }),
    catchError(err => {
        this.errorMsg(err.message)
        return "";
    }));
  }

  private _prepareResponseData(data: any): any {
    return data
  }

  private _get(domen: string, path: string, get_params = {}, custom_headers?: IHeader[]): Observable<Object> {
    let headers = this._headers;
    custom_headers?.forEach((item: IHeader) => headers.set(item.name, item.value));

    return this._http.get(
      this.prepareURL(domen, path), {
      headers: headers,
      params: new HttpParams({fromObject: get_params})
    })
  }

  private _post(
    domen: string, path: string, post_data = {}, get_params = {}, custom_headers?: IHeader[]
  ): Observable<any> {
    let headers = this._headers;

    custom_headers?.forEach((item: IHeader) => {
      headers.append(item.name, item.value);
    });

    return this._http.post(
      this.prepareURL(domen, path),
      post_data, {
        headers: headers,
        params: new HttpParams({fromObject: get_params})
      }
    )
  }
}
