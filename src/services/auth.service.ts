import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { HttpService, IHeader, RequestMethod } from "./http.service";
// @ts-ignore
import { Subscription } from "rxjs/src/internal/Subscription";
import { NotificationService } from "./notification.service";

export interface IServerResponse {
  success: boolean;
  message: string;
  error?: string;
}

export interface IListResponse extends IServerResponse {
  result: any[]
}

export interface IViewResponse extends IServerResponse {
  result: any;
}

export interface IGetCardResponse extends IServerResponse {
  data: object
}

export interface ICheckCodeResponse extends IServerResponse {
  user: IUser;
  token: string;
}

export interface ICardSavedResponse extends IServerResponse {
  representation: string;
}

export interface IUser {
  email: string;
  name: string;
  is_superuser: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public isAuthorized: boolean = false;
  public isLoggedIn: boolean = false;
  public redirectURL!: string;
  public isLoading: boolean = false;
  private _formHeader: IHeader[];
  private _user: IUser | null = null;
  private _tokenName: string = "Token";

  constructor(
    public notification: NotificationService,
    private _router: Router,
    private _http: HttpClient,
    private _httpService: HttpService
  ) {
    this._formHeader = [
      { name: "Content-Type", value: "application/x-www-form-urlencoded" }
    ];

    if (this._checkValidToken()) {
      this.isAuthorized = true;
      this.isLoggedIn = true;
    }
  }

  public get userName(): string | undefined {
    return this.user?.name;
  }

  public get token(): string | null {
    return localStorage.getItem(this._tokenName);
  }

  public set token(value: string | null) {
    this._httpService.setHeader("token", value);

    if (value) {
      localStorage.setItem(this._tokenName, value);
    } else {
      localStorage.removeItem(this._tokenName);
    }
  }

  public get user(): IUser | null {
    if (this._user) {
      return this._user;
    }

    let user = localStorage.getItem("user");

    if (user) {
      return JSON.parse(user);
    }
    return null
  }

  public set user(user: IUser | null) {
    this._user = user;
    localStorage.setItem("user", JSON.stringify(user));
  }

  public login(email: string, password: string): void {
    this.isLoading = true;
    let response = this._httpService.requestAdmin(
      "/login",
      RequestMethod.POST,
      {email: email, password: password},
      {only_status: true},
      this._formHeader
    );

    response.subscribe((data: IServerResponse) => {
      this.isLoading = false;
      this.isAuthorized = data.success;

      if (!data.success) {
        this.notification.warning(data.error);
      }
    });
  }

  public sendCode(code: string, email: string): void {
    this.isLoading = true;
    let response = this._httpService.requestAdmin(
      "/check_code", RequestMethod.POST, {code: code, email: email},
      {only_status: true}, this._formHeader
    );

    response.subscribe((data: ICheckCodeResponse) => {
      this.isLoading = false;
      this.isLoggedIn = this.isAuthorized && data.success;

      if (data.success) {
        this.user = data.user;
        this.token = data.token;
        this._router.navigate([this.redirectURL ? this.redirectURL : ""]).then()
      } else {
        this.notification.warning(data.error);
      }
    });
  }

  public resendCode(email: string): void {
    this.isLoading = true;
    let response = this._httpService.requestAdmin(
      "/resend_code", RequestMethod.POST, {email: email}, {only_status: true}, this._formHeader
    );

    response.subscribe((data: IServerResponse) => {
      this.isLoading = false;
      this.notification.notificate(data.message);
    });
  }

  public logout(): void {
    this.isLoading = true;
    this.isLoggedIn = false;
    this.isAuthorized = false;
    this.user = null;
    this.token = null;

    let response = this._httpService.requestAdmin("/admin_logout", RequestMethod.POST, {}, {only_status: true});

    response.subscribe((data: IServerResponse) => {
      this.isLoading = false;
      this._router.navigate(["login"]).then();
    });
  }

  private _checkValidToken(): boolean {
    return !!this.token;
  }
}
