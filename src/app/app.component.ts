import { Component } from '@angular/core';
import { AuthService } from "../services/auth.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public title: string = 'my_user_name';

  constructor(
    public authService: AuthService
  ) {
  }

  public logout(): void {
    this.authService.logout();
  }

  public isLoggedIn(): boolean {
    return this.authService.isLoggedIn;
  }

}
