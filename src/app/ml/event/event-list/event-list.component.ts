import { AfterViewInit, Component } from '@angular/core';
import { BaseGridComponent } from "../../../../base/crud-grid-component";


export interface IEvent {
  id: number;
  dt: Date;
  load_statistic: boolean;
  h_tn: string;
  h_tid: number;
  v_tn: string;
  v_tid: number;
  markets_team: string;
  send_markets: boolean;
  full_auto: boolean;
  enable_resolve: boolean;
}

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent extends BaseGridComponent implements AfterViewInit {
  public override prepareColumns(): string[] {
    return [
      "id", "dt", "h_tn", "v_tn", "markets_team", "load_statistic", "send_markets", "full_auto", "enable_resolve"
    ]
  }

  public override getEntityName(): string {
    return "events"
  }

  public marketTeamWidget(value: string): string {
    let data = {
      "v": "Away",
      "h": "Home",
      "m": "Multiteam"
    };

    // @ts-ignore
    return data[value] || value;
  }
}
