import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { HttpService, RequestMethod } from "../../../../services/http.service";
import { IListResponse, IViewResponse } from "../../../../services/auth.service";
import { NotificationService } from "../../../../services/notification.service";
import { FormControl } from "@angular/forms";

export interface IEventView {
  awayTeamName: string;
  homeTeamName: string;
  awayScore: number;
  homeScore: number;
  time: string;
  markets_count: number;
  error_count: number;
}

export interface ILog {
  id: number;
  datetime: Date;
  data: string;
}

@Component({
  selector: 'app-event-view',
  templateUrl: './event-view.component.html',
  styleUrls: ['./event-view.component.css']
})
export class EventViewComponent implements OnInit {
  public id!: number;
  public event!: IEventView;
  public logs!: ILog[];
  public tabs = new FormControl(0);
  private _selectedErrorLogId!: number | null;

  constructor(
    public notification: NotificationService,
    protected httpService: HttpService,
    private _activatedRoute: ActivatedRoute
  ) {
    this._activatedRoute.params.subscribe(params => {
      this.id = params['id'];
    });
  }

  ngOnInit(): void {
    this._initDataSource();
    this._initLogsData();
  }

  public representation(): string {
    if (!this.event) {
      return "";
    }
    return this.event.homeTeamName + " vs " + this.event.awayTeamName + " at " + this.event.time;
  }

  public selectLogErrorById(id: number): void {
    this.tabs.setValue(4);
    this._selectedErrorLogId = id;
  }

  public checkAnchor(): string {
    if (this._selectedErrorLogId) {
      document.getElementById("event-log-" + this._selectedErrorLogId)?.scrollIntoView();
    }
    return "";
  }

  public triggerErrorLogSelectedClass(log: ILog): boolean {
    return log.id == this._selectedErrorLogId
  }

  public getView(): EventViewComponent {
    return this
  }

  private _initDataSource(): void {
    let response = this.httpService.requestML(
      "/events/" + this.id, RequestMethod.GET
    );

    response.subscribe((data: IViewResponse) => {
      if (data.error) {
        this.notification.warning(data.error);
      }
      if (data.success) {
        this.event = data.result;
      }
    });
  }

  private _initLogsData(): void {
    let response = this.httpService.requestML(
      "/events/" + this.id + "/logs", RequestMethod.GET
    );

    response.subscribe((data: IListResponse) => {
      if (data.error) {
        this.notification.warning(data.error);
      }
      if (data.success) {
        this.logs = data.result;
      }
    });
  }
}
