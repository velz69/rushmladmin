import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventMarketsComponent } from './event-markets.component';

describe('EventMarketsComponent', () => {
  let component: EventMarketsComponent;
  let fixture: ComponentFixture<EventMarketsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EventMarketsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EventMarketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
