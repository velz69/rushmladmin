import { AfterViewInit, Component, Input } from '@angular/core';
import { BaseGridComponent } from "../../../../base/crud-grid-component";
import { FormControl } from "@angular/forms";
import { EventViewComponent } from "../event-view/event-view.component";


export interface IMarket {
  id: number;
  period: number;
  class_name: string;
  status: string; // (value: publish/unpublish/resolve/re-resolve/cancel)
  title: string;
  first_condition: string;
  second_condition: string;
  form_time: Date;
  resolve: number;
  resolve_actionNumber: number; // (ссылка на строку с  actionNumber в раздел со статистикой pbp )
  resolve_time: Date
  error_id: number;
}


@Component({
  selector: 'app-event-markets',
  templateUrl: './event-markets.component.html',
  styleUrls: ['./event-markets.component.css']
})
export class EventMarketsComponent extends BaseGridComponent implements AfterViewInit {
  @Input() public event_id!: number;
  @Input() public event_view!: EventViewComponent;

  public statusPicker!: FormControl;
  public statusList = ["Publish", "Unpublished", "Resolve", "Re-resolve", "Cancel"];

  public override prepareColumns(): string[] {
    return [
      "id", "title", "first_condition", "second_condition", "class_name", "status", "form_time",
      "resolve", "resolve_actionNumber", "resolve_time", "error_id"
    ]
  }

  public override getEntityName(): string {
    return "events/" + this.event_id + "/markets";
  }

  public selectErrorLog(log_id?: number): void {
    if (!log_id) {
      return
    }
    this.event_view.selectLogErrorById(log_id)
  }

  public resolveActionNumberLink(id?: number): string {
    if (id) {
      return "<a class='nav-link' [routerLink]='#event-log-" + id + "'>link</a>"
    }
    return ""
  }

  public resolveWidget(value: string): string {
    let data = {
      0: "First",
      1: "Second",
      2: "Unpublish",
      3: "Cancel"
    };

    // @ts-ignore
    return data[value] || value;
  }
}
