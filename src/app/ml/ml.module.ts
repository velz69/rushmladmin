import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MlRoutingModule } from './ml-routing.module';
import { MatTableModule } from "@angular/material/table";
import { MatPaginatorModule } from "@angular/material/paginator";
import { EventModule } from "./event/event.module";
import { MarketConfigModule } from "./market-config/market-config.module";
import { TriviaConfigModule } from "./trivia-config/trivia-config.module";


@NgModule({
  declarations: [],
  exports: [],
  imports: [
    CommonModule,
    MlRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    EventModule,
    MarketConfigModule,
    TriviaConfigModule
  ]
})
export class MlModule { }
