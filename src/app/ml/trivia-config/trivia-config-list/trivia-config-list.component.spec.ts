import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TriviaConfigListComponent } from './trivia-config-list.component';

describe('TriviaConfigListComponent', () => {
  let component: TriviaConfigListComponent;
  let fixture: ComponentFixture<TriviaConfigListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TriviaConfigListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TriviaConfigListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
