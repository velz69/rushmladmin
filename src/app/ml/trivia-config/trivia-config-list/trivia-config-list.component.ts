import { AfterViewInit, Component } from '@angular/core';
import { BaseCrudGridComponent } from "../../../../base/crud-grid-component";
import { TriviaConfigCardComponent } from "../trivia-config-card/trivia-config-card.component";

export interface ITriviaConfig {
  id: number;
  tag: string;
  difficulty: number;
  title: string;
  answer_a: string;
  answer_b: string;
  note: string;
  res: number;
  type: string;
  lifetime: number;
  bonus: number;
  show_percentage: number;
}

@Component({
  selector: 'app-trivia-config-list',
  templateUrl: './trivia-config-list.component.html',
  styleUrls: ['./trivia-config-list.component.css']
})
export class TriviaConfigListComponent extends BaseCrudGridComponent implements AfterViewInit {
  public override getEntityName(): string {
    return "trivia_config";
  }

  public override prepareColumns(): string[] {
    return [
      "id", "tag", "title", "answer_a", "answer_b", "difficulty", "res", "lifetime", "bonus", "note"
    ]
  }

  public override getCardComponent(): any {
    return TriviaConfigCardComponent
  }
}
