import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TriviaConfigCardComponent } from './trivia-config-card.component';

describe('TriviaConfigCardComponent', () => {
  let component: TriviaConfigCardComponent;
  let fixture: ComponentFixture<TriviaConfigCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TriviaConfigCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TriviaConfigCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
