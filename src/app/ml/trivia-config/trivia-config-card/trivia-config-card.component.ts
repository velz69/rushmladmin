import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { ITriviaConfig } from "../trivia-config-list/trivia-config-list.component";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { HttpService, RequestMethod } from "../../../../services/http.service";
import { ICardSavedResponse } from "../../../../services/auth.service";
import { NotificationService } from "../../../../services/notification.service";

@Component({
  selector: 'app-trivia-config-card',
  templateUrl: './trivia-config-card.component.html',
  styleUrls: ['./trivia-config-card.component.css']
})
export class TriviaConfigCardComponent {
  public cardForm: FormGroup;

  constructor(
    public notifications: NotificationService,
    protected _httpService: HttpService,
    @Inject(MAT_DIALOG_DATA) public trivia_config: ITriviaConfig
  ) {
    let controls = {
      "id": new FormControl(trivia_config?.id),
      "tag": new FormControl(trivia_config?.tag, [Validators.required]),
      "difficulty": new FormControl(trivia_config?.difficulty || 1, [Validators.required]),
      "title": new FormControl(trivia_config?.title, [Validators.required]),
      "answer_a": new FormControl(trivia_config?.answer_a, [Validators.required]),
      "answer_b": new FormControl(trivia_config?.answer_b, [Validators.required]),
      "note": new FormControl(trivia_config?.note, [Validators.required]),
      "res": new FormControl(trivia_config?.res || 0, [Validators.required]),
      "type": new FormControl(trivia_config?.type || "BONUS", [Validators.required]),
      "lifetime": new FormControl(trivia_config?.lifetime || 25, [Validators.required]),
      "bonus": new FormControl(trivia_config?.bonus || 50, [Validators.required]),
      "show_percentage": new FormControl(trivia_config?.show_percentage || 0, [Validators.required])
    };

    this.cardForm = new FormGroup(controls);
  }

  public save(): void {
    let action = "create",
      url = "trivia_config/";

    if (this.cardForm.value.id) {
      action = "/update";
      url += this.cardForm.value.id;
    }

    let response = this._httpService.requestML(
      url + action, RequestMethod.POST,
      this.cardForm.value, { id: this.cardForm.value?.id }
    );

    response.subscribe((data: ICardSavedResponse) => {
      if (data.error) {
        this.notifications.warning(data.error);
      }
      if (data.success) {
        this.notifications.notificate("Trivia config " + action + "d: " + data.representation);
      }
    });
  }
}
