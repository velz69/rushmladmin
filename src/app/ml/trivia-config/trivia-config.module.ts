import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TriviaConfigListComponent } from './trivia-config-list/trivia-config-list.component';
import { TriviaConfigCardComponent } from './trivia-config-card/trivia-config-card.component';
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatTableModule } from "@angular/material/table";
import { MatButtonModule } from "@angular/material/button";
import { MatDialogModule } from "@angular/material/dialog";
import { ReactiveFormsModule } from "@angular/forms";



@NgModule({
  declarations: [
    TriviaConfigListComponent,
    TriviaConfigCardComponent
  ],
	imports: [
		CommonModule,
		MatPaginatorModule,
		MatTableModule,
		MatButtonModule,
		MatDialogModule,
		ReactiveFormsModule
	]
})
export class TriviaConfigModule { }
