import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventViewComponent } from "./event/event-view/event-view.component";
import { EventListComponent } from "./event/event-list/event-list.component";
import { MarketConfigListComponent } from "./market-config/market-config-list/market-config-list.component";
import { TriviaConfigListComponent } from "./trivia-config/trivia-config-list/trivia-config-list.component";
import { CanActivateGuard } from "../../guards/can-activate.guard";

const routes: Routes = [
  { path: "", redirectTo: "api/events/list", pathMatch: "full" },
  { path: "api/events", canActivate: [CanActivateGuard],
    children: [
      { path: "list", component: EventListComponent },
      { path: ":id", component: EventViewComponent },
    ]
  },
  { path: "api/markets_config/list", component: MarketConfigListComponent, canActivate: [CanActivateGuard] },
  { path: "api/trivia_config/list", component:  TriviaConfigListComponent, canActivate: [CanActivateGuard] },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class MlRoutingModule { }
