import { AfterViewInit, Component } from '@angular/core';
import { BaseCrudGridComponent } from "../../../../base/crud-grid-component";
import { MarketConfigCardComponent } from "../market-config-card/market-config-card.component";

export interface IMarketConfig {
  id: number;
  group: string;
  subgroup: string;
  status: number;
  class_name: string;
  difficulty: number;
  excitement: boolean;
  player_market: boolean;
  both_teams: boolean;
  type: string;
  prize: number;
  live_time: number;
  reaction_time: number;
  show_percentage: boolean;
  show_lucky_cheers: boolean;
  proba_auto: number;
  question: string;
  option1: string;
  option2: string;
}

@Component({
  selector: 'app-market-config-list',
  templateUrl: './market-config-list.component.html',
  styleUrls: ['./market-config-list.component.css']
})
export class MarketConfigListComponent extends BaseCrudGridComponent implements AfterViewInit {
  public override getEntityName(): string {
    return "markets_config";
  }

  public override prepareColumns(): string[] {
    return [
      "id", "status", "group", "subgroup", "class_name", "difficulty", "type",
      "prize", "live_time", "reaction_time", "proba_auto", "question", "option1", "option2",
      "show_percentage", "excitement", "player_market", "both_teams", "show_lucky_cheers",
    ]
  }

  public override getCardComponent(): any {
    return MarketConfigCardComponent;
  }

}
