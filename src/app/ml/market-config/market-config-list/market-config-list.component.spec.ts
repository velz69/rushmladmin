import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarketConfigListComponent } from './market-config-list.component';

describe('MarketConfigListComponent', () => {
  let component: MarketConfigListComponent;
  let fixture: ComponentFixture<MarketConfigListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarketConfigListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarketConfigListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
