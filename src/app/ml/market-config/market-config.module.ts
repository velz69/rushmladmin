import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarketConfigListComponent } from './market-config-list/market-config-list.component';
import { MarketConfigCardComponent } from './market-config-card/market-config-card.component';
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatTableModule } from "@angular/material/table";
import { RouterModule } from "@angular/router";
import { MatButtonModule } from "@angular/material/button";
import { MatDialogModule } from "@angular/material/dialog";
import { ReactiveFormsModule } from "@angular/forms";



@NgModule({
  declarations: [
    MarketConfigListComponent,
    MarketConfigCardComponent
  ],
  imports: [
    CommonModule,
    MatPaginatorModule,
    MatTableModule,
    RouterModule,
    MatButtonModule,
    MatDialogModule,
    ReactiveFormsModule
  ]
})
export class MarketConfigModule { }
