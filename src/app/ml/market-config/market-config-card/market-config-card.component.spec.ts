import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarketConfigCardComponent } from './market-config-card.component';

describe('MarketConfigCardComponent', () => {
  let component: MarketConfigCardComponent;
  let fixture: ComponentFixture<MarketConfigCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarketConfigCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarketConfigCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
