import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { IMarketConfig } from "../market-config-list/market-config-list.component";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { HttpService, RequestMethod } from "../../../../services/http.service";
import { ICardSavedResponse } from "../../../../services/auth.service";
import { NotificationService } from "../../../../services/notification.service";

@Component({
  selector: 'app-market-config-card',
  templateUrl: './market-config-card.component.html',
  styleUrls: ['./market-config-card.component.css']
})
export class MarketConfigCardComponent {
  public cardForm: FormGroup;

  constructor(
    public notifications: NotificationService,
    protected _httpService: HttpService,
    @Inject(MAT_DIALOG_DATA) public market_config?: IMarketConfig
  ) {
    let controls = {
      "id": new FormControl(market_config?.id),
      "group": new FormControl(market_config?.group, [Validators.required]),
      "subgroup": new FormControl(market_config?.subgroup, [Validators.required]),
      "status": new FormControl(market_config?.status || 1, [Validators.required]),
      "class_name": new FormControl(market_config?.class_name, [Validators.required]),
      "difficulty": new FormControl(market_config?.difficulty || 1, [Validators.required]),
      "excitement": new FormControl(market_config?.excitement || false),
      "player_market": new FormControl(market_config?.player_market || false),
      "both_teams": new FormControl(market_config?.both_teams || false),
      "type": new FormControl(market_config?.type || "BONUS", [Validators.required]),
      "prize": new FormControl(market_config?.prize || 10, [Validators.required]),
      "live_time": new FormControl(market_config?.live_time || 0, [Validators.required]),
      "reaction_time": new FormControl(market_config?.reaction_time, [Validators.required]),
      "show_percentage": new FormControl(market_config?.show_percentage || true),
      "show_lucky_cheers": new FormControl(market_config?.show_lucky_cheers || true),
      "proba_auto": new FormControl(market_config?.proba_auto, [Validators.required]),
      "question": new FormControl(market_config?.question, [Validators.required]),
      "option1": new FormControl(market_config?.option1, [Validators.required]),
      "option2": new FormControl(market_config?.option2, [Validators.required])
    }

    this.cardForm = new FormGroup(controls);
  }

  public save(): void {
    let action = "create",
      url = "markets_config/";

    if (this.cardForm.value.id) {
      action = "/update";
      url += this.cardForm.value.id;
    }

    let response = this._httpService.requestML(
      url + action, RequestMethod.POST,
      this.cardForm.value, { id: this.cardForm.value?.id }
    );

    response.subscribe((data: ICardSavedResponse) => {
      if (data.error) {
        this.notifications.warning(data.error);
      }
      if (data.success) {
        this.notifications.notificate("Market config " + action + "d: " + data.representation);
      }
    });
  }
}
