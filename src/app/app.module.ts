import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { LayoutsComponent } from './layouts/layouts.component';
import { ToolbarComponent } from './layouts/toolbar/toolbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { HeaderComponent } from './layouts/header/header.component';
import { MlModule } from "./ml/ml.module";
import { RouterModule } from "@angular/router";
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from "./app-routing/app-routing.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AuthService } from "../services/auth.service";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { ProgressBarComponent } from './layouts/progress-bar/progress-bar.component';
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatDialog, MatDialogModule } from "@angular/material/dialog";
import { BaseCrudGridComponent, BaseGridComponent } from "../base/crud-grid-component";
import { NotificationService } from "../services/notification.service";
import { MatSnackBar } from "@angular/material/snack-bar";

@NgModule({
  declarations: [
    AppComponent,
    LayoutsComponent,
    ToolbarComponent,
    FooterComponent,
    HeaderComponent,
    LoginComponent,
    ProgressBarComponent,
  ],
	imports: [
		BrowserModule,
		MlModule,
		RouterModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		HttpClientModule,
		FormsModule,
		MatFormFieldModule,
		ReactiveFormsModule,
		MatIconModule,
		MatProgressBarModule,
    MatDialogModule
	],
  providers: [
    LoginComponent,
    AuthService,
    NotificationService,
    {
       provide: MatDialog
     },
    {
       provide: MatSnackBar
     },
    BaseGridComponent,
    BaseCrudGridComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
