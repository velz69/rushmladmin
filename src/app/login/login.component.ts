import { Component } from '@angular/core';
import { AuthService } from "../../services/auth.service";
import { Router } from "@angular/router";
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  public loginForm: FormGroup;
  public codeForm: FormGroup;

  constructor(
    public authService: AuthService,
    private _router: Router
  ) {
    this.loginForm = new FormGroup({
      "email": new FormControl("", [Validators.required, Validators.email]),
      "password": new FormControl("", [Validators.required]),
    });

    this.codeForm = new FormGroup({
      "code": new FormControl("", [Validators.required]),
    });
  }

  public login(): void {
    this.authService.login(this.loginForm.value.email, this.loginForm.value.password);
  }

  public sendCode(): void {
    this.authService.sendCode(this.codeForm.value.code, this.loginForm.value.email);
  }

  public resendCode(): void {
    this.authService.resendCode(this.loginForm.value.email);
  }

  public logout(): void {
    this.authService.logout();
  }
}
